﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Shootingscript : MonoBehaviour
{
    float xRot, yRot = 0f;
    public Rigidbody rb;
    public float rotationSpeed = 5f;
    public GameObject player;
    public float shootingPower = 10f;
    public LineRenderer Line;

    public static int shootingcharge = 2;

    public Text ChargeText;
    // Update is called once per frame
    void Update()
    {
        ChargeText.text = shootingcharge.ToString(); 
        transform.position = rb.position;

        if(Input.GetMouseButton(1))
        {
            xRot += Input.GetAxis("Mouse X") * rotationSpeed;
            yRot += Input.GetAxis("Mouse Y") * rotationSpeed;
            if(yRot < -50f)
            {
                yRot = -50f;
            }
            if(yRot > 50f)
            {
                yRot = 50f;
            }
            transform.rotation = Quaternion.Euler(-yRot, xRot, 0f);

            Line.gameObject.SetActive(true);
            Line.SetPosition(0, transform.position);
            Line.SetPosition(1, transform.position + transform.forward * 4f);

        }
        if (Input.GetMouseButtonUp(1))
        {
            //    Line.gameObject.SetActive(true);
            //    Line.SetPosition(0, transform.position);
            //    Line.SetPosition(1, transform.position + transform.forward *4f);
            Line.gameObject.SetActive(false);
        }
        
            

        if (Input.GetMouseButtonUp(0) && shootingcharge >= 1)
        {
            rb.velocity = transform.forward * shootingPower;
            
            shootingcharge--;
        }
        
        
    }
    
}
