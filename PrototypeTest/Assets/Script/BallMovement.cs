﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BallMovement : MonoBehaviour
{
    public float speed;
    private Rigidbody rb;

    float moveHorizontal;
    float moverVertical;
    Vector3 moverment;

    [SerializeField]
    Slider _slider;
    public float _impulseForce;

    bool isPushing = false;
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }
    

    // Update is called once per frame
    void FixedUpdate()
    {
        

        moveHorizontal = Input.GetAxis("Horizontal");
        moverVertical = Input.GetAxis("Vertical");

        moverment = new Vector3(moveHorizontal, 0.0f, moverVertical);
        rb.AddForce(moverment * speed, ForceMode.Force);


        
        if (Input.GetKeyDown(KeyCode.Space))
        {
            _slider.value = 0;
        }
        else if (Input.GetKey(KeyCode.Space))
        {
            _slider.value += Time.deltaTime * 15;
        }
        else if (Input.GetKeyUp(KeyCode.Space))
        {
            _impulseForce = _slider.value;

            if (isPushing == false)
            {
                rb.AddForce(moverment * _impulseForce, ForceMode.Impulse);
            }
 
            rb.collisionDetectionMode = CollisionDetectionMode.ContinuousDynamic;


        }
        if(isPushing == true)
        {
            if (Input.GetMouseButtonDown(0))
            {
                speed = 7;
                rb.WakeUp();
                //Debug.Log("WakeUp");

                rb.AddForce(this.transform.position, ForceMode.Impulse);
                rb.collisionDetectionMode = CollisionDetectionMode.ContinuousDynamic;
            }
        }
        

    }
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Push"))
        {
            isPushing = true;
            speed = 0;
            rb.Sleep();
            Debug.Log("Click mouse Left!!");
        }
        
    }
    void OnTriggerExit(Collider other)
    {
        isPushing = false;
    }


}
