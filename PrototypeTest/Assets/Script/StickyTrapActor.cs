﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StickyTrapActor : MonoBehaviour
{
    FixedJoint _fixedJoint = null;
    bool isJointing=false;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (isJointing == true)
        {
            if (Input.GetMouseButtonDown(1))
            {
                Debug.Log("ReleaseFixedJoint");
                ReleaseFixedJoint();
            }
        }
        

    }

    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Sticky"))
        {
            isJointing = true;
            Debug.Log("Click mouse Right!!");
            GameObject go = other.gameObject;
            StickyTrap gluetrap = go.GetComponent<StickyTrap>();

                _fixedJoint = this.gameObject.AddComponent<FixedJoint>();
                _fixedJoint.connectedBody = other.gameObject.GetComponent<Rigidbody>();

            
                
            
        }
        

    }

    void ReleaseFixedJoint()
    {
        _fixedJoint.connectedBody = null;
        Destroy(_fixedJoint);
    }
}
